angular.module("tutorialCtrlModule",[])

.controller("TutorialCtrl",["$scope","Calculations",function ($scope,Calculations) {
	
	$scope.tutorialObject = [];
	$scope.tutorialObject.title = "Main Page";
	$scope.tutorialObject.subTitle = "Sub Page";
	$scope.tutorialObject.firstName = "Siddhant";
	$scope.tutorialObject.lastName = "Chaudhari";
	$scope.tutorialObject.bindOutput = 2;
	$scope.timesTwo = function () {
		$scope.tutorialObject.bindOutput = Calculations.timesTwo($scope.tutorialObject.bindOutput);
	}
	Calculations.pythagorianTheorem()
}])

.directive("welcomeMessage",function(){
	return {
		restrict: "AE",
		template: "<div>Howdy ! How are you ?</div>"
	}
})

.factory("Calculations",function(){
	var calculations = {};
	calculations.timesTwo = function(a){
		return a * 2;
	};

	calculations.pythagorianTheorem = function(a,b){
		return (a * a) + (b * b);
	};
	return calculations;
});
